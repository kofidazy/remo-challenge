import * as React from 'react';
import './Theater.scss';
import MapImage from '../assets/conference-map.svg';
import TableConfig from './tableConfig.json';
import firebase from '../services/firebase';
import { useHistory } from 'react-router-dom';
import { useEffect } from 'react';

const Theater: React.FC = () => {
  const history = useHistory();
  const [tables, setTables] = React.useState([])
  const [user, setUser] = React.useState<any>(null)
  const [logoutState, setLogoutState] = React.useState(false)
  const [popUp, setPopUp] = React.useState(false)
  const [locate, setLocate] = React.useState("")
  const [message, setMessage] = React.useState("")

  /**
   * Displays an error message at the top of the screen
   * @param message information text for pop up
   */
  const showPopUp = (message: string) => {
    setMessage(message)
    setPopUp(true)
    setTimeout(() => {
      setPopUp(false)
    }, 10000);
  }

  /**
   * Finds a suitable table to add the new user to
   * @param tables current state of the tables. Arrray<>
   * @param user new user to be added
   */
  const addUser = (tables: Array<any>, user: any) => {
    let minimum = 2
    let complete = false
    while (!complete) {
      for (let index = 0; index < tables.length; index++) {
        const table = tables[index];

        if (!table.hasOwnProperty("users")) {
          table.users = []
        }

        if (table.users.length < minimum) {
          if (table.maxSeats > table.users.length) {
            appendTable(table, index, user)
            complete = true
            break;
          } else {
            console.log("Table is probably full")
          }
        }

        if ((index + 1) == tables.length) {
          if ((table.users.length == table.maxSeats) && minimum === 6) {
            showPopUp("This room is at maximum capacity")
            complete = true
            break;
          } else {
            minimum = minimum + 1
            index = -1;
            continue;
          }
        }
      }
    }
  }

  /**
   * Attach new user to table if possible
   * @param table selected table
   * @param index seating position
   * @param user user to be added
   */
  const appendTable = (table: any, index: number, user: any) => {
    if (!table.hasOwnProperty("users")) {
      table.users = []
    }
    table.users = [...table.users, {
      email: user.email,
      image: user.photoURL
    }]
    firebase.database().ref(`room/${index}`).set(table)
    readRoom()
  }

  /**
   * Switch tables for user if possible
   * @param user switchable user
   * @param tableId destination table
   */
  const switchTable = (user: any, tableId: String) => {
    const newTable: any = tables.find((table: any) => table.id === tableId);
    if (!newTable.hasOwnProperty("users")) {
      newTable.users = []
    }
    if (newTable.maxSeats > newTable.users.length) {
      leaveRoom(user)
      appendTable(newTable, tables.findIndex((table: any) => table.id === tableId), user)
    } else {
      showPopUp("This table is at maximum capacity")
    }
  }

  /**
   * Read the state of the firebase tables once
   */
  const readRoom = () => {
    const room = firebase.database().ref('room')
    room.once('value', function (snapshot) {
      const roomTables = snapshot.val();
      setTables(roomTables);
    })
  }

  /**
   * Monitor the state of the tables and fetch in new 
   * changes when posible
   */
  const observeTables = () => {
    firebase.database().ref('room').on('value', function (snapshot) {
      const roomTables = snapshot.val();
      setTables(roomTables);
    });
  }

  useEffect(() => {
    firebase.auth().onAuthStateChanged((authenticatedUser) => {
      if (authenticatedUser) {
        const email = authenticatedUser.email
        setUser(authenticatedUser)
        const room = firebase.database().ref('room')
        room.once('value', function (snapshot) {
          const roomTables = snapshot.val();
          setTables(roomTables);
          let userFound = false;
          roomTables.map((table: any) => {
            if (table.users) {
              for (let index = 0; index < table.users.length; index++) {
                const usr = table.users[index];
                if (usr) {
                  if (email === usr.email) {
                    userFound = true;
                    break;
                  }
                }
              }
            }
          })

          if (!userFound) {
            // TODO if not add user to next available table
            addUser(roomTables, authenticatedUser)
          }

        })
      } else {
        history.push('/');
      }
    });

    observeTables()

  }, [user, logoutState])

  /**
   * Remove user from the table 
   * @param user selected user
   */
  const leaveRoom = (user: any) => {
    setLogoutState(true)
    tables.map((table: any, tableIndex: number) => {
      let triggerUpdate = false
      if (table.users) {
        const newUsers = []
        for (let index = 0; index < table.users.length; index++) {
          const usr = table.users[index];
          if (usr) {
            if (user.email === usr.email) {
              firebase.database().ref(`room/${tableIndex}/users/` + index).remove()
              triggerUpdate = true
            } else {
              newUsers.push(usr)
            }
          }
        }

        if (triggerUpdate) {
          table.users = [...newUsers]
          firebase.database().ref(`room/${tableIndex}`).set(table)
        }

      }
    })
  }

  /**
   * Just a utility method to test user placement
   */
  const addRandomUsers = () => {
    let count = 1
    while (count < 20) {
      fetch('https://randomuser.me/api/')
        .then(response => response.json())
        .then(data => {
          const user = data.results[0]
          console.log(user)
          addUser(tables, {
            email: user.email,
            photoURL: user.picture.thumbnail
          });
        })
      count++
    }
  }

  /**
   * Sign out user from firebase auth
   * @param user user to be signed out
   */
  const signOut = (user: any) => {
    leaveRoom(user)
    firebase.auth().signOut()
  };

  return (
    user ? <div>
      <div className='rt-app-bar'>
        <div>
          <span className="vertical-center"><img className='small-thumbnail' src={user?.photoURL} /></span>
          <div className="dropdown ">
            <span className="user-details">{user?.displayName}</span>
            <div className="dropdown-content">
              <p className="logout" onClick={() => setLocate("enlarge")}>Locate</p>
              <p className="logout" onClick={() => addRandomUsers()}>Add 25 users</p>
              <p className="logout" onClick={() => signOut(user)}>Logout</p>
            </div>
          </div>
        </div>
      </div>

      {popUp && <div className="popUp">{message}</div>}
      <div className='remo-theater' style={{ width: TableConfig.width, height: TableConfig.height }}>

        <div className='rt-rooms'>
          {
            tables.map((table: any, index: number) => {
              return <div
                onDoubleClick={() => switchTable(user, table.id)}
                key={index}
                className='rt-room'
                style={{ width: table.width, height: table.height, top: table.y, left: table.x }}>
                {table.hasOwnProperty('users') && table.users.map((usr: any, index: number) => {
                  return <span
                    key={index}
                    className="image-container">
                    {user.email === usr.email && <span className="star">&#9733;</span>}
                    <img
                      src={usr.image}
                      className={`small-thumbnail ${user.email === usr.email && locate}`}
                      onAnimationEnd={() => setLocate("")}
                      style={{ marginLeft: table.seats[index].x, marginTop: table.seats[index].y }} />
                  </span>
                })
                }
                <div className='rt-room-name'>{table.id}</div>
              </div>
            })
          }


        </div>
        <div className='rt-background'>
          <img src={MapImage} alt='Conference background' />
        </div>
      </div>
    </div> : null
  );
};

export default Theater;